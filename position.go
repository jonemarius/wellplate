package wellplate

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Position stores the position on a well plate
// according to "human" indices, (1-based, not 0-based)
type Position struct {
	Row    uint
	Column uint
}

// SliceCoord returns the position of the position in
// 0-based coordinates (useful for slices/arrays)
func (p *Position) SliceCoord() (row int, column int) {
	return int(p.Row) - 1, int(p.Row) - 1
}

// String() implements the Stringer interface and returns the
// position as a string where 1,1 will return A1
func (p Position) String() string {
	return fmt.Sprintf("%s%d", string(rune(int(p.Row)+64)), p.Column)
}

// GoString implements the Gostringer interface
func (p Position) GoString() string {
	return fmt.Sprintf("Position{Row:%d, Column:%d}", p.Row, p.Column)
}

const rowGroupname = "Row"
const columnGroupname = "column"

// See https://regex101.com/r/7HVzx9/1
var posRegex = regexp.MustCompile(fmt.Sprintf(`^(?P<%s>[a-h]|[A-H])(?P<%s>[0][1-9]|1[1-2]|[1-9]|10)$`, rowGroupname, columnGroupname))

func validPosition(posStr string) error {
	if !posRegex.Match([]byte(posStr)) {
		return fmt.Errorf("%s is not a valid position string", posStr)
	}

	return nil
}

func rowFromString(rowStr string) uint {
	return uint(strings.ToUpper(rowStr)[0]) - 64
}

// PositionFromString will return an initialized Position based on
// a string input on the usual form "A1-H12", "A01-H12" or lowercase versions
// of these
func PositionFromString(posStr string) (Position, error) {
	err := validPosition(posStr)
	if err != nil {
		return Position{}, err
	}
	// If it is valid, we also know that the posRegex has found
	// both a valid character describing row position and a column position
	pos := posRegex.FindStringSubmatch(posStr)
	// Due to the regex,checking for errors from strconv.Atoi is not necessary
	col, _ := strconv.Atoi(pos[2])
	return Position{Row: rowFromString(pos[1]), Column: uint(col)}, nil
}

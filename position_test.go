package wellplate

import (
	"reflect"
	"testing"
)

func TestPosition_SliceCoord(t *testing.T) {
	type fields struct {
		Row    uint
		Column uint
	}
	tests := []struct {
		name       string
		fields     fields
		wantRow    int
		wantColumn int
	}{
		{
			name: "1,1 returns 0,0",
			fields: fields{
				Row:    1,
				Column: 1,
			},
			wantRow:    0,
			wantColumn: 0,
		},
		{
			name: "2,2 returns 1,1",
			fields: fields{
				Row:    2,
				Column: 2,
			},
			wantRow:    1,
			wantColumn: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Position{
				Row:    tt.fields.Row,
				Column: tt.fields.Column,
			}
			gotRow, gotColumn := p.SliceCoord()
			if gotRow != tt.wantRow {
				t.Errorf("Position.SliceCoord() gotRow = %v, want %v", gotRow, tt.wantRow)
			}
			if gotColumn != tt.wantColumn {
				t.Errorf("Position.SliceCoord() gotColumn = %v, want %v", gotColumn, tt.wantColumn)
			}
		})
	}
}

func TestPosition_String(t *testing.T) {
	type fields struct {
		Row    uint
		Column uint
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "1,1 returns A1",
			fields: fields{
				Row:    1,
				Column: 1,
			},
			want: "A1",
		},
		{
			name: "2,2 returns B2",
			fields: fields{
				Row:    2,
				Column: 2,
			},
			want: "B2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Position{
				Row:    tt.fields.Row,
				Column: tt.fields.Column,
			}
			if got := p.String(); got != tt.want {
				t.Errorf("Position.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPosition_GoString(t *testing.T) {
	type fields struct {
		Row    uint
		Column uint
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Position{Row:1, Column:1} should output exactly that",
			fields: fields{
				Row:    1,
				Column: 1,
			},
			want: "Position{Row:1, Column:1}",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Position{
				Row:    tt.fields.Row,
				Column: tt.fields.Column,
			}
			if got := p.GoString(); got != tt.want {
				t.Errorf("Position.GoString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_validPosition(t *testing.T) {
	type args struct {
		posStr string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "A1 is a valid position string",
			args: args{
				posStr: "A1",
			},
			wantErr: false,
		},
		{
			name: "A01 is a valid position string",
			args: args{
				posStr: "A01",
			},
			wantErr: false,
		},
		{
			name: "AB is an invalid position string",
			args: args{
				posStr: "AB",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validPosition(tt.args.posStr); (err != nil) != tt.wantErr {
				t.Errorf("validPosition() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_rowFromString(t *testing.T) {
	type args struct {
		rowStr string
	}
	tests := []struct {
		name string
		args args
		want uint
	}{
		{
			name: "A should return 1",
			args: args{
				rowStr: "A",
			},
			want: 1,
		},
		{
			name: "a should return 1",
			args: args{
				rowStr: "a",
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := rowFromString(tt.args.rowStr)
			if got != tt.want {
				t.Errorf("rowFromString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPositionFromString(t *testing.T) {
	type args struct {
		posStr string
	}
	tests := []struct {
		name    string
		args    args
		want    Position
		wantErr bool
	}{
		{
			name: "A1 should result in (1,1)",
			args: args{
				posStr: "A1",
			},
			want: Position{
				Row:    1,
				Column: 1,
			},
			wantErr: false,
		},
		{
			name: "A01 should result in (1,1)",
			args: args{
				posStr: "A01",
			},
			want: Position{
				Row:    1,
				Column: 1,
			},
			wantErr: false,
		},
		{
			name: "a01 should result in (1,1)",
			args: args{
				posStr: "a01",
			},
			want: Position{
				Row:    1,
				Column: 1,
			},
			wantErr: false,
		},
		{
			name: "A13 should result in error",
			args: args{
				posStr: "A13",
			},
			want: Position{
				Row:    0,
				Column: 0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := PositionFromString(tt.args.posStr)
			if (err != nil) != tt.wantErr {
				t.Errorf("PositionFromString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PositionFromString() = %v, want %v", got, tt.want)
			}
		})
	}
}
